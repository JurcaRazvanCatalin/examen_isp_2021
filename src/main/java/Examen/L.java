import java.util.ArrayList;
import java.util.List;

public class L {
    private M[] m;

    private int a;

    public L(){

        m = new M[2];
        m[0] = new M(new ArrayList<B>());
        m[1] = new M(new ArrayList<B>());
    }

    public void i(X x){}
    public String w(){
        return("Text");
    }
}

class X {
    X(L l){
        System.out.println(l.w());
    }
}

class M {

    private ArrayList<B> b = new ArrayList<B>();

    public M(ArrayList<B> b) {

        this.b = b;
    }

    public void addStudent(B b) {
        this.b.add(b);
    }

}

class B {

    public void metB() {
    }
}

class A extends C {

    List<M> m;

    public void metA() {
    }
}

class C {}
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Ex1 extends JFrame {

    private JTextField textField,textField1;
    private JButton button;

    Ex1(){

        setTitle("Ex1");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        init();
        setLayout(null);
        setSize(500,500);
    }

    public JTextField getTextField() {
        return textField;
    }

    public void setTextField(JTextField textField) {
        this.textField = textField;
    }

    public JTextField getTextField1() {
        return textField1;
    }

    public void setTextField1(JTextField textField1) {
        this.textField1 = textField1;
    }

    public JButton getButton() {
        return button;
    }

    public void setButton(JButton button) {
        this.button = button;
    }

    public void init(){

        textField = new JTextField();
        textField.setBounds(50,50,300,100);
        add(textField);

        textField1 = new JTextField();
        textField1.setBounds(50,170,300,100);
        add(textField1);

        button = new JButton("Button Apasata-n");
        button.setBounds(50,270,300,50);
        add(button);
    }

    public static class B {
        private Ex1 ex;

        public B(Ex1 ex){

            this.ex = ex;
            ex.getButton().addActionListener(e ->{

                String aux = ex.getTextField().getText();
                if(aux.length() == 0){
                    ex.getTextField().setText("Apasat-an");
                    ex.getTextField1().setText("");
                }else{
                    ex.getTextField().setText("");
                    ex.getTextField1().setText("Apasat-an");
                }
            });
        }

    }

    public static void main(String[] args) {

        Ex1 c = new Ex1();
        B b = new B(c);
    }

}
